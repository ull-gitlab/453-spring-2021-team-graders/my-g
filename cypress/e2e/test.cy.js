var stud_firstname = 'Student FN';
var stud_lastname = 'Student LN';
var stud_email =
    'student_' +
    Math.random()
        .toString(36)
        .replace(/[^a-z]+/g, '')
        .substr(0, 9) +
    '@email.com';
var stud_password = (Math.random().toString(36) + '00000000000000000').slice(2, 12);

var teach_firstname = 'Teacher FN';
var teach_lastname = 'Teacher LN';
var teach_email =
    'teacher_' +
    Math.random()
        .toString(36)
        .replace(/[^a-z]+/g, '')
        .substr(0, 9) +
    '@email.com';
var teach_password = (Math.random().toString(36) + '00000000000000000').slice(2, 12);

describe('Register student', () => {
    it('Test student register button', () => {
        cy.visit('/accounts/');
        cy.contains("I'm a student").click();
        cy.url().should('include', '/accounts/signup/student');
    });
    it('Test student register form', () => {
        cy.register(
            '/accounts/signup/student/',
            stud_firstname,
            stud_lastname,
            stud_email,
            stud_password
        );
    });
});

describe('Register teacher', () => {
    it('Test teacher register button', () => {
        cy.visit('/accounts/');
        cy.contains("I'm a teacher").click();
        cy.url().should('include', '/accounts/signup/teacher');
    });
    it('Test Register Form', () => {
        cy.register(
            '/accounts/signup/teacher/',
            teach_firstname,
            teach_lastname,
            teach_email,
            teach_password
        );
    });
});
