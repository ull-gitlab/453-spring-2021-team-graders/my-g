"""Config for polls app."""
from django.apps import AppConfig


class MygConfig(AppConfig):
    """Myg app config."""

    name = "myg"
