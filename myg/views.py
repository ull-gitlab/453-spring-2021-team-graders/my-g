"""myg app views."""
from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import redirect, render
from django.urls import reverse_lazy
from django.views import generic

from myg import forms, models
from users import mixins


class CourseList(generic.ListView):
    """Course list view."""

    model = models.Course


class CourseDetail(LoginRequiredMixin, generic.DetailView):
    """Course detail view."""

    model = models.Course


class CourseCreate(mixins.TeacherRequiredMixin, generic.CreateView):
    """View to create course."""

    model = models.Course
    fields = ["title"]
    success_url = reverse_lazy("myg:course-list")

    def form_valid(self, form: object) -> object:
        """Override the form valid method to store the current user as teacher."""
        course_obj = form.save(commit=False)
        course_obj.teacher = self.request.user
        return super().form_valid(form)


class CourseUpdate(mixins.TeacherRequiredMixin, generic.UpdateView):
    """View to update a Course."""

    model = models.Course
    fields = ["title"]
    success_url = reverse_lazy("myg:course-list")

    def dispatch(self, request, *args, **kwargs):
        """Override dispatch method.

        Redirect if the current user is not the owner of course.

        """
        if self.get_object().teacher != self.request.user:
            return redirect("myg:course-list")
        return super().dispatch(request, *args, **kwargs)


class AssessmentList(mixins.TeacherStudentRequiredMixin, generic.ListView):
    """Assessment list view."""

    model = models.Assessment

    def get_context_data(self, **kwargs):
        """Add additional context to display the course title in the template."""
        context = super().get_context_data(**kwargs)
        context["course"] = models.Course.objects.filter(id=self.kwargs["course_id"]).get()
        return context

    def get_queryset(self, **kwargs):
        """Only display assessments related to the current course."""
        queryset = super().get_queryset(**kwargs)
        return queryset.filter(course=self.kwargs["course_id"])


class AssesmentCreate(mixins.TeacherRequiredMixin, generic.CreateView):
    """View to create assessment."""

    model = models.Assessment
    fields = ["assessment_type", "title", "description"]

    def get_success_url(self):
        """Get success url."""
        course_id = self.kwargs["course_id"]
        return reverse_lazy("myg:assessment-list", kwargs={"course_id": course_id})

    def form_valid(self, form: object) -> object:
        """Override form valid method to store the current user as the teacher field value."""
        assessment = form.save(commit=False)
        assessment.course_id = models.Course.objects.filter(id=self.kwargs["course_id"]).get().id
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        """Add additional context to display the course title in the template."""
        context = super().get_context_data(**kwargs)
        context["course"] = models.Course.objects.filter(id=self.kwargs["course_id"]).get()
        return context


class AssessmentDetail(mixins.TeacherStudentRequiredMixin, generic.DetailView):
    """Assessment detail view."""

    model = models.Assessment

    def get_object(self, queryset=None):
        """Get the object of the assessment detail."""
        if queryset is None:
            queryset = self.get_queryset()

        return queryset.filter(course__id=self.kwargs["course_id"], id=self.kwargs["pk"]).get()

    def dispatch(self, request, *args, **kwargs):
        """Override dispatch method.

        Redirect to assessment list if the current user is not one of these two:
        1. Teacher who teaches the course.
        2. Student who has enrolled for the course.

        """
        if (
            self.request.user.is_teacher and self.get_object().course.teacher != self.request.user
        ) or (
            self.request.user.is_student
            and self.request.user not in self.get_object().course.students.all()
        ):
            return redirect(
                reverse_lazy(
                    "myg:assessment-list",
                    kwargs={"course_id": self.get_object().course.id},
                )
            )
        return super().dispatch(request, *args, **kwargs)


class AssessmentUpdate(mixins.TeacherRequiredMixin, generic.UpdateView):
    """Assessment detail Update view."""

    model = models.Assessment
    fields = ["title", "description"]

    def get_object(self, queryset=None):
        """Get the object of the assessment that is being updated."""
        if queryset is None:
            queryset = self.get_queryset()

        return queryset.filter(course__id=self.kwargs["course_id"], id=self.kwargs["pk"]).get()

    def get_success_url(self):
        """Get success url: Redirect to detail page of the assessment that is being updated."""
        return reverse_lazy(
            "myg:assessment-detail",
            kwargs={"course_id": self.get_object().course.id, "pk": self.get_object().id},
        )

    def dispatch(self, request, *args, **kwargs):
        """Dispatch method: Check if the current user is the teacher of the course."""
        if self.get_object().course.teacher != self.request.user:
            return redirect(
                reverse_lazy(
                    "myg:assessment-list",
                    kwargs={"course_id": self.get_object().course.id},
                )
            )

        return super().dispatch(request, *args, **kwargs)


class EnrollmentCreate(mixins.StudentRequiredMixin, generic.UpdateView):
    """Enroll for a course."""

    model = models.Course
    template_name = "myg/enrollment_form.html"
    fields = []

    def get_success_url(self):
        """Get success url: Redirect to detail page of the course."""
        course_id = self.kwargs["pk"]
        return reverse_lazy("myg:course-detail", kwargs={"pk": course_id})

    def form_valid(self, form: object) -> object:
        """Override form valid method to store the current user as the teacher field value."""
        course = form.save()
        course.students.add(self.request.user)
        return super().form_valid(form)


class EntrollmentList(mixins.TeacherRequiredMixin, generic.DetailView):
    """List of enrolled students."""

    model = models.Course
    template_name = "myg/enrollment_list.html"


class AssessmentGradeCreate(mixins.TeacherRequiredMixin, generic.FormView):
    """View to create or upgrade grade for an assessment."""

    form_class = forms.AssessmentGradeForm
    fields = ["student", "grade"]
    template_name = "myg/assessment_grade_form.html"

    def get_form(self, *args, **kwargs):
        """In the form, only display registered students of the course."""
        form = super().get_form(*args, **kwargs)
        course = models.Course.objects.filter(id=self.kwargs["course_id"]).get()
        form.fields["student"].queryset = course.students.all()
        return form

    def get_context_data(self, **kwargs):
        """Add additional context to display the course and assessment title in the template."""
        context = super().get_context_data(**kwargs)
        context["assessment"] = models.Assessment.objects.filter(id=self.kwargs["course_id"]).get()
        context["course"] = models.Course.objects.filter(id=self.kwargs["course_id"]).get()
        return context

    def post(self, request, *args, **kwargs):
        """Override get method to check if given data already exists, then redirect."""
        assessment_id = self.kwargs["assessment_id"]
        course_id = self.kwargs["course_id"]
        form = self.form_class(request.POST)

        if form.is_valid():
            student_id = form.cleaned_data.get("student").id
            grade = form.cleaned_data.get("grade")

            models.Grade.objects.update_or_create(
                assessment_id=assessment_id, student_id=student_id, grade=grade
            )

            return redirect(
                reverse_lazy(
                    "myg:assessment-list",
                    kwargs={"course_id": course_id},
                )
            )

        return render(request, self.template_name, {"form": form})

    def get_success_url(self):
        """Get success url."""
        course_id = self.kwargs["course_id"]
        assessment_id = self.kwargs["assessment_id"]
        return reverse_lazy(
            "myg:assessment-grade-list",
            kwargs={"course_id": course_id, "assessment_id": assessment_id},
        )


class AssessmentGradeList(mixins.TeacherRequiredMixin, generic.ListView):
    """List the grades of all the assessments of a course."""

    model = models.Grade
    template_name = "myg/assessment_grade_list.html"

    def get_queryset(self, **kwargs):
        """Only display assessments related to the current course."""
        queryset = super().get_queryset(**kwargs)
        return queryset.filter(assessment_id=self.kwargs["assessment_id"])

    def get_context_data(self, **kwargs):
        """Add additional context to display the course title in the template."""
        context = super().get_context_data(**kwargs)
        context["course"] = models.Course.objects.filter(id=self.kwargs["course_id"]).get()
        context["assessment"] = models.Assessment.objects.filter(
            id=self.kwargs["assessment_id"]
        ).get()
        return context


class AssessmentGradeUpdate(mixins.TeacherRequiredMixin, generic.UpdateView):
    """Update the grade of an assessments of a course."""

    model = models.Grade
    template_name = "myg/assessment_grade_form.html"
    fields = ["student", "grade"]

    def get_form(self, *args, **kwargs):
        """In the form, only display registered students of the course."""
        form = super().get_form(*args, **kwargs)
        course = models.Course.objects.filter(id=self.kwargs["course_id"]).get()
        form.fields["student"].queryset = course.students.all()
        return form

    def get_context_data(self, **kwargs):
        """Add additional context to display the course title in the template."""
        context = super().get_context_data(**kwargs)
        context["course"] = models.Course.objects.filter(id=self.kwargs["course_id"]).get()
        context["assessment"] = models.Assessment.objects.filter(
            id=self.kwargs["assessment_id"]
        ).get()
        return context

    def get_success_url(self):
        """Get success url."""
        course_id = self.kwargs["course_id"]
        assessment_id = self.kwargs["assessment_id"]
        return reverse_lazy(
            "myg:assessment-grade-list",
            kwargs={"course_id": course_id, "assessment_id": assessment_id},
        )


class StudentGradeList(mixins.TeacherStudentRequiredMixin, generic.ListView):
    """List the grades recevied by a student in all courses and their assessments."""

    model = models.Grade


class GradeDetail(mixins.TeacherStudentRequiredMixin, generic.DetailView):
    """Grade detail view."""

    model = models.Grade
