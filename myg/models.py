"""MyG models."""
from django.db import models

from users.models import CustomUser


class Course(models.Model):
    """Course model class.

    Attributes:
    ----------
    title : string
        Holds the title of the course
    semester: string
        The semester in which the course is offered.
    teacher: foreign key
        Holds a reference to the CustomUser class object.

    """

    title = models.CharField(max_length=200)
    teacher = models.ForeignKey(CustomUser, on_delete=models.CASCADE)
    students = models.ManyToManyField(CustomUser, related_name="course_students", blank=True)

    def __str__(self) -> str:
        """Return a string representation of Question."""
        return f"{self.title} offered by {self.teacher}"


class Assessment(models.Model):
    """Assesment model class.

    Attributes:
    ----------
    course : foreign key
        Holds a reference to the Course class object.
    assessment_type : string
        Holds a string that represents the type of the assesment.

    """

    class AssessmentChoices(models.IntegerChoices):
        """Assessment choice model."""

        EXAM = 1
        QUIZ = 2
        ASSIGNMENT = 3
        HOMEWORK = 4
        PROJECT = 5

    course = models.ForeignKey(Course, on_delete=models.CASCADE)
    assessment_type = models.IntegerField(
        choices=AssessmentChoices.choices, default=AssessmentChoices.PROJECT
    )
    title = models.CharField(max_length=200)
    description = models.TextField(default="")

    def __str__(self) -> str:
        """Return a string representation of assessment."""
        return f"[{self.assessment_type}] {self.title}"


class Grade(models.Model):
    """Grade model class."""

    class GradeChoices(models.TextChoices):
        """Grade choices."""

        EXCELLENT = "A"
        GOOD = "B"
        AVERAGE = "C"
        POOR = "D"

    student = models.ForeignKey(CustomUser, on_delete=models.CASCADE)
    grade = models.CharField(max_length=2, choices=GradeChoices.choices, default=GradeChoices.POOR)
    assessment = models.ForeignKey(Assessment, on_delete=models.CASCADE)

    class Meta:
        """Meta class."""

        unique_together = ("student", "assessment")

    def __str__(self) -> str:
        """Return a string representation of grade."""
        return f"{self.student}:{self.assessment}"
