"""Admin app."""
from django.contrib import admin

# Register your models here.
from myg.models import Assessment, Course, Grade

admin.site.register(Course)
admin.site.register(Grade)
admin.site.register(Assessment)
