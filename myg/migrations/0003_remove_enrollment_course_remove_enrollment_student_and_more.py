# Generated by Django 4.1.7 on 2023-03-20 17:21

from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ("myg", "0002_assessment_course_enrollment_grade_delete_choice_and_more"),
    ]

    operations = [
        migrations.RemoveField(
            model_name="enrollment",
            name="course",
        ),
        migrations.RemoveField(
            model_name="enrollment",
            name="student",
        ),
        migrations.AddField(
            model_name="enrollment",
            name="course",
            field=models.ManyToManyField(to="myg.course"),
        ),
        migrations.AddField(
            model_name="enrollment",
            name="student",
            field=models.ManyToManyField(to=settings.AUTH_USER_MODEL),
        ),
    ]
