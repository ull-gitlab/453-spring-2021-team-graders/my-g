"""myg app URL."""
from django.urls import path

from myg import views

app_name = "myg"

urlpatterns = [
    path("", views.CourseList.as_view(), name="course-list"),
    path("course/add/", views.CourseCreate.as_view(), name="course-create"),
    path("course/<int:pk>/", views.CourseDetail.as_view(), name="course-detail"),
    path("course/<int:pk>/update", views.CourseUpdate.as_view(), name="course-update"),
    path(
        "course/<int:course_id>/assessment/",
        views.AssessmentList.as_view(),
        name="assessment-list",
    ),
    path(
        "course/<int:course_id>/assessment/add/",
        views.AssesmentCreate.as_view(),
        name="assessment-create",
    ),
    path(
        "course/<int:course_id>/assessment/<int:pk>/",
        views.AssessmentDetail.as_view(),
        name="assessment-detail",
    ),
    path(
        "course/<int:course_id>/assessment/<int:pk>/update/",
        views.AssessmentUpdate.as_view(),
        name="assessment-update",
    ),
    path(
        "course/<int:pk>/enrollment/",
        views.EntrollmentList.as_view(),
        name="enrollment-list",
    ),
    path(
        "course/<int:pk>/enrollment/add/",
        views.EnrollmentCreate.as_view(),
        name="enrollment-create",
    ),
    path(
        "course/<int:course_id>/assessment/<int:assessment_id>/grades/",
        views.AssessmentGradeList.as_view(),
        name="assessment-grade-list",
    ),
    path(
        "course/<int:course_id>/assessment/<int:assessment_id>/grades/add/",
        views.AssessmentGradeCreate.as_view(),
        name="assessment-grade-create",
    ),
    path(
        "course/<int:course_id>/assessment/<int:assessment_id>/grades/<int:pk>/",
        views.GradeDetail.as_view(),
        name="grade-detail",
    ),
    path(
        "course/<int:course_id>/assessment/<int:assessment_id>/grades/<int:pk>/update/",
        views.AssessmentGradeUpdate.as_view(),
        name="assessment-grade-update",
    ),
]
