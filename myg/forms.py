"""Forms for accounts app."""
from django.forms import ModelForm

from myg import models


class AssessmentGradeForm(ModelForm):
    """Registration form."""

    class Meta:
        """Meta class."""

        model = models.Grade
        fields = ("student", "grade")
